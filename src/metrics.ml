(* Utils *)

let read_line ic =
  let buf = Buffer.create 128 in
  let rec loop () =
    Lwt.try_bind
      (fun () -> Lwt_io.read_char ic)
      (function
        | '\r' ->
            Lwt.return (Buffer.contents buf)
        | ch ->
            Buffer.add_char buf ch ; loop ())
      (function
        | End_of_file -> Lwt.return (Buffer.contents buf) | exn -> Lwt.fail exn)
  in
  Lwt_io.read_char ic
  >>= function '\r' -> loop () | ch -> Buffer.add_char buf ch ; loop ()

let mk_in_channel () =
  let socket = Lwt_unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  Lwt_unix.connect
    socket
    (Unix.ADDR_INET (Unix.inet_addr_of_string Config.addr, Config.port))
  >>= fun () ->
  let in_chan = Lwt_io.of_fd ~mode:Lwt_io.Input socket in
  Lwt.return in_chan

(* Metrics *)

open Prometheus

let namespace = "tezos_metrics"

module Energy = struct
  let subsystem = "energy"

  module Volt = struct
    let name = "volt"

    let help = "volt"

    let st = Gauge.v ~namespace ~help name

    let set v = Gauge.set st v
  end

  module Ampere = struct
    let name = "ampere"

    let help = "ampere"

    let st = Gauge.v ~namespace ~help name

    let set v = Gauge.set st v
  end

  module Watt = struct
    let name = "watt"

    let help = "watt"

    let st = Gauge.v ~namespace ~help name

    let set v = Gauge.set st v
  end

  module Watt_hour = struct
    let name = "watt_hour"

    let help = "watt hour"

    let st = Gauge.v ~namespace ~help name

    let set v = Gauge.set st v
  end
end

let decode s = Scanf.sscanf s "%fV, %fA, %fW, %fWh"

let reboot delay f =
  Format.eprintf "Unexpected error while proceeding@." ;
  Format.eprintf "Waiting %.0fs before trying again@." delay ;
  Lwt_unix.sleep (min 60. delay) >>= fun () -> f ()

let rec run delay : unit Lwt.t =
  let rec get_channel delay =
    Lwt.catch mk_in_channel (fun _ ->
        reboot delay (fun () -> get_channel (2. *. delay)))
  in
  get_channel delay
  >>= fun in_chan ->
  let rec loop () : unit Lwt.t =
    read_line in_chan
    >>= fun s ->
    decode s (fun v a w wh ->
        Energy.Volt.set v ;
        Energy.Ampere.set a ;
        Energy.Watt.set w ;
        Energy.Watt_hour.set wh) ;
    loop ()
  in
  Lwt.catch loop (fun _ -> reboot delay (fun () -> run Config.delay))

let run () = run Config.delay
