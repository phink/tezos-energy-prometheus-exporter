let main prometheus_config =
  let threads = Metrics.run () :: Prometheus_unix.serve prometheus_config in
  Lwt_main.run (Lwt.choose threads)

let () =
  let open Cmdliner in
  let spec = Term.(const main $ Prometheus_unix.opts) in
  let info = Term.info "tezos energy metrics" in
  match Term.eval (spec, info) with `Error _ -> exit 1 | _ -> exit 0
