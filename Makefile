build:
	dune build @main
clean:
	dune clean
run:
	@./_build/default/src/main.exe --listen-prometheus=11991
